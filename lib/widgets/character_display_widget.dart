import 'package:battle_tec/models/ability.dart';
import 'package:battle_tec/models/character.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'character_ability_widget.dart';

class CharacterDisplayWidget extends StatelessWidget {
  const CharacterDisplayWidget({
    Key key,
    @required this.character,
  }) : super(key: key);

  final Character character;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: 15),
              Image(
                image: AssetImage('assets/factions/${character.faction}.png'),
                width: 75,
              ),
              Expanded(
                child: Text(
                  "${character.name}",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 28,
                  ),
                ),
                flex: 3,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Points
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/money_t.png'),
                width: 30,
              ),
              Text(
                "Points:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.points}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Energy
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/gem_t.png'),
                width: 30,
              ),
              Text(
                "Entergy:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.energy}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Stragety
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/rank_t.png'),
                width: 30,
              ),
              Text(
                "Stratgey Rating:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  "${character.stratgeyRating}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Movement
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/curs_01_t.png'),
                width: 30,
              ),
              Text(
                "Movement:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.movement}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Shields
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/armor_t.png'),
                width: 30,
              ),
              Text(
                "Shields:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.shields}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Ranged Attack
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/target_t.png'),
                width: 30,
              ),
              Text(
                "Ranged Attack:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.rangedAttack}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Close Attack
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/war_t.png'),
                width: 30,
              ),
              Text(
                "Close Attack:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.closeAttack}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              // Health
              SizedBox(
                width: 75,
              ),
              Image(
                image: AssetImage('assets/icons/heart_t.png'),
                width: 30,
              ),
              Text(
                "Health:",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "${character.damage}",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              // Energy
              SizedBox(
                width: 50,
              ),
              Expanded(
                flex: 2,
                child: Text(
                  "Ablities",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                width: 75,
              ),
            ],
          ),
          Expanded(
            child: CharacterAbilityWidget(character: character),
          ),
        ],
      ),
    );
  }
}
