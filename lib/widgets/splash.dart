import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  final int seconds;
  final Widget navigateDesitnation;
  final ImageProvider backgroundImage;
  const SplashScreen({
    Key key,
    this.seconds,
    this.navigateDesitnation,
    this.backgroundImage,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: widget.seconds), () {
      Navigator.of(context).pushReplacement(_createRoute());
    });
  }

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          widget.navigateDesitnation,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return FadeTransition(opacity: animation, child: child);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: (widget.backgroundImage == null)
            ? null
            : DecorationImage(
                image: widget.backgroundImage,
                fit: BoxFit.cover,
              ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: 500),
          CircularProgressIndicator(),
        ],
      ),
    );
  }
}
