import 'package:battle_tec/models/ability.dart';
import 'package:battle_tec/models/character.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CharacterAbilityWidget extends StatelessWidget {
  
  const CharacterAbilityWidget({
    Key key,
    @required this.character,
    this.showAsAlert = false,
  }) : super(key: key);

  final Character character;
  final bool showAsAlert;

  Future<void> _showAlertDialog(BuildContext context, String title, String body) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(body),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: character.notes.length,
        itemBuilder: (context, index) {
          Ability ability = Provider.of<AbilityProvider>(context)
              .getAbilityByName(character.notes[index]);
          if (ability == null) {
            ability = Ability.empty();
          }
          return ListTile(
            title: Text(
              "${ability.name}",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: ability.isSuper ? 24 : 18,
                  fontWeight: ability.isSuper ? FontWeight.bold : null),
            ),
            onTap: () {
              if(showAsAlert){
                _showAlertDialog(context, ability.name, "${ability.desciption} \n\n energy cost: ${ability.cost}");
              }else{
                SnackBar snackbar = SnackBar(
                  content: Text(ability.desciption),
                );
                Scaffold.of(context).showSnackBar(snackbar);
              }
            },
          );
        });
  }
}
