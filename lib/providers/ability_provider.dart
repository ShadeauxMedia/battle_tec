import 'dart:collection';
import 'dart:convert';

import 'package:battle_tec/core/asset_manager.dart';
import 'package:battle_tec/models/ability.dart';
import 'package:flutter/material.dart';

import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constiants.dart';


class AbilityProvider extends ChangeNotifier with AssetManager{

  
  Box _box;
  int _selected_index;

  Future _loadAbilities() async{
    var pref = await SharedPreferences.getInstance();
    Hive.registerAdapter(AbilityAdapter());
    _box = await Hive.openBox<Ability>(ABILITIES_BOX_NAME);
    if(!pref.containsKey(ABILITY_FIRST_USE_KEY)){
      List data = jsonDecode(await loadAsset('assets/fixtures/abilities.json'));
      data.forEach((json){
        Ability ability = Ability.fromJSON(json);
        _box.put(ability.name.hashCode, ability);
        //_abilities[ability.name] = ability;
      });
      pref.setBool(ABILITY_FIRST_USE_KEY, false);
    }else{
      //_box.values.
    }
    notifyListeners();
  }

  List<String> get abilityList{
    List<String> keys = [];
    if(_box != null){
      keys.addAll(_box.values.map((e) => e.name));
      keys.sort((a, b) => a.compareTo(b));
    }
    return keys;
  }

  UnmodifiableListView<Ability> get abilities {
    if(_box != null){
      List<Ability> temp = _box.values.toList();
      temp.sort(
        (a, b) => a.name.compareTo(b.name)
      );
      return UnmodifiableListView(temp);
    }else
      return UnmodifiableListView([]);
  }

  int get count{
    if(_box == null) return 0;
    return _box.length;
  }

  AbilityProvider(){
    _loadAbilities();
  }

  Ability getAbilityByName(String name){
    if(_box != null)
      return _box.get(name.hashCode);
    else
      return Ability.empty();
  }

  void setSelected(int index){
    _selected_index = index;
  }
  
  Ability getSelected(){
    return _box.get(_selected_index);
  }

  void saveAbility(Ability ability) {
    _box.put(ability.name.hashCode, ability);
    notifyListeners();
  }
  void deleteAbility(int key){
    _box.delete(key);
    notifyListeners();
  }
}