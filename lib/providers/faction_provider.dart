import 'dart:collection';
import 'dart:convert';

import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/core/asset_manager.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';


class FactionProvider extends ChangeNotifier with AssetManager{
  Box _box;
    
  UnmodifiableListView<String> get factions {
    if(_box != null){
      List<String> temp = _box.values.toList();
      temp.sort(
        (a, b) => a.compareTo(b)
      );
      return UnmodifiableListView(temp);

    }else  
      return UnmodifiableListView([]);
  }

  FactionProvider(){
    loadFactionsFromFile();
  }

  void loadFactionsFromFile() async{
     var pref = await SharedPreferences.getInstance();
    _box = await Hive.openBox<String>(FACTIONS_BOX_NAME);
    if(!pref.containsKey(FACTION_FIRST_USE_KEY)){
      List data = jsonDecode(await loadAsset('assets/fixtures/factions.json'));
      data.forEach((json){
        _box.add(json);
      });
      pref.setBool(FACTION_FIRST_USE_KEY, false);
    }
    notifyListeners();
  }
}