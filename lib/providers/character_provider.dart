import 'dart:convert';

import 'package:battle_tec/models/character.dart';
import 'package:flutter/material.dart';
import 'dart:collection';
import 'package:battle_tec/core/asset_manager.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constiants.dart';

class CharacterProvider extends ChangeNotifier with AssetManager{
  Box _box;

  
  int _selected_index = 0;

  UnmodifiableListView<Character> get characters {
    if(_box != null){
      List<Character> temp = _box.values.toList();
      temp.sort(
        (a, b) => a.name.compareTo(b.name)
      );
      return UnmodifiableListView(temp);
    }else
      return UnmodifiableListView([]);
  }

  CharacterProvider(){
    loadCharactersFromFile();
  }

  void loadCharactersFromFile() async{
    var pref = await SharedPreferences.getInstance();
    Hive.registerAdapter(CharacterAdapter());
    _box = await Hive.openBox<Character>(CHARACTERS_BOX_NAME);
    if(!pref.containsKey(CHARACTER_FIRST_USE_KEY)){
      List data = jsonDecode(await loadAsset('assets/fixtures/starter.json'));
      data.forEach((json){
        Character character = Character.fromJSON(json);
        _box.put(character.name.hashCode, character);
      });
      pref.setBool(CHARACTER_FIRST_USE_KEY, false);
    }
    notifyListeners();
  }

  void addCharacter(Character character) {
    // saveCharacter
    _box.put(character.name.hashCode, character);
    notifyListeners();
  }

  void setSelected(int index){
    _selected_index = index;
  }
  
  Character getSelected(){
    return _box.get(_selected_index);
  }

  List<Character> filterCharactersByNames(List<String> names){
    List<Character> temp = _box.values.where((character) => names.contains(character.name)).toList();
    temp.sort((a, b) => a.stratgeyRating.compareTo(b.stratgeyRating));
    return temp;
  }

  void deleteCharacter(Character character){
    _box.delete(character.name.hashCode);
    notifyListeners();
  }
}