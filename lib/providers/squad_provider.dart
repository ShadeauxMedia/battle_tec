import 'dart:collection';
import 'dart:convert';

import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/core/asset_manager.dart';
import 'package:flutter/material.dart';
import 'package:battle_tec/models/squad.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SquadProvider extends ChangeNotifier with AssetManager {
  Box _box;
  int _selected_index = 0;

  UnmodifiableListView<Squad> get squads {
    if(_box != null)
      return UnmodifiableListView(_box.values);
    else
      return UnmodifiableListView([]);
  }

  SquadProvider() {
    loadSquads();
  }

  Future<void> loadSquads() async {
    var pref = await SharedPreferences.getInstance();
    Hive.registerAdapter(SquadAdapter());
    _box = await Hive.openBox<Squad>(SQUADS_BOX_NAME);
    if(!pref.containsKey(SQUAD_FIRST_USE_KEY)){
      List data = jsonDecode(await loadAsset('assets/fixtures/squads.json'));
      
      data.forEach((json) {
        Squad squad = Squad.fromJSON(json);
        _box.put(squad.name.hashCode, squad);  
      });
      pref.setBool(SQUAD_FIRST_USE_KEY, false);
    }
    notifyListeners();
  }

  void setSelected(int index){
    _selected_index = index;
  }

  Squad getSelected(){
    return _box.get(_selected_index);
  }

  void addSquad(Squad squad) {
    _box.put(squad.name.hashCode, squad);
    notifyListeners();
  }

  void deleteSquad(Squad squad) {
    _box.delete(squad.name.hashCode);
    notifyListeners();
  }
}
