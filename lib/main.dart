import 'package:battle_tec/providers/ability_provider.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:battle_tec/providers/faction_provider.dart';
import 'package:battle_tec/providers/squad_provider.dart';
import 'package:battle_tec/screens/ability_list_screen.dart';
import 'package:battle_tec/screens/about_screen.dart';
import 'package:battle_tec/screens/home_screen.dart';
import 'package:battle_tec/screens/squad_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:battle_tec/screens/character_list_screen.dart';
import 'package:battle_tec/screens/squad_list_screen.dart';
import 'package:battle_tec/screens/battle_screen.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

void main() => runApp(BattleTecApp());

class BattleTecApp extends StatelessWidget {
  // This widget is the root of your application.
  BattleTecApp(){
    init();
  }
  Future init() async{
    await Hive.initFlutter();
  }
  
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => CharacterProvider()),
          ChangeNotifierProvider(create: (_) => AbilityProvider()),
          ChangeNotifierProvider(create: (_) => FactionProvider()),
          ChangeNotifierProvider(create: (_) => SquadProvider()),
        ],
        child: MaterialApp(
          title: 'Battle Tec',
          theme: ThemeData.dark().copyWith(
            primaryColor: Colors.red,
            accentColor: Colors.redAccent,
            cardColor: Colors.transparent,
            floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: Colors.transparent
            ),
            tabBarTheme: TabBarTheme(
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          // home: CharacterListScreen(),
          routes: {
            "home": (context) =>HomeScreen(),
            "squads" : (context) => SquadListScreen(),
            "squad_edit": (context) => SquadEditScreen(),
            "battle" : (context) => BattleScreen(),
            "characters": (context) => CharacterListScreen(),
            "abilities": (context) => AbilityListScreen(),
            "about": (context) => AboutScreen(),
          },
          initialRoute: "home",
        ));
  }
}
