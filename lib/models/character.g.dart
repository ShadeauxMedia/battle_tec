// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CharacterAdapter extends TypeAdapter<Character> {
  @override
  final typeId = 1;

  @override
  Character read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Character(
      name: fields[0] as String,
      faction: fields[1] as String,
      notes: (fields[2] as List)?.cast<String>(),
      points: fields[3] as int,
      energy: fields[4] as int,
      stratgeyRating: fields[5] as int,
      movement: fields[6] as int,
      damage: fields[7] as int,
      rangedAttack: fields[8] as int,
      closeAttack: fields[9] as int,
      shields: fields[10] as int,
    );
  }

  @override
  void write(BinaryWriter writer, Character obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.faction)
      ..writeByte(2)
      ..write(obj.notes)
      ..writeByte(3)
      ..write(obj.points)
      ..writeByte(4)
      ..write(obj.energy)
      ..writeByte(5)
      ..write(obj.stratgeyRating)
      ..writeByte(6)
      ..write(obj.movement)
      ..writeByte(7)
      ..write(obj.damage)
      ..writeByte(8)
      ..write(obj.rangedAttack)
      ..writeByte(9)
      ..write(obj.closeAttack)
      ..writeByte(10)
      ..write(obj.shields);
  }
}
