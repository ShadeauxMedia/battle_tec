
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'squad.g.dart';

@HiveType(typeId: 2)
class Squad extends Equatable{

  @HiveField(0)
  String name;
  @HiveField(1)
  String faction;
  @HiveField(2)
  int totalPoints;
  @HiveField(3)
  List<String> members =[];

  Squad({this.name, this.faction, this.totalPoints, this.members});


  @override
  // TODO: implement props
  List<Object> get props => [name];
  
  factory Squad.fromJSON(Map<String, dynamic> json){
    var data = json['members'] as List;
    Squad squad = Squad(
      name: json["name"],
      faction: json['faction'],
      totalPoints: json["totalPoints"],
      members: data.cast<String>(),
    );
    return squad;
  }

  Map<String, dynamic> toJSON(){
    Map<String, dynamic> data = {};
    data["name"] = name;
    data['faction'] = faction;
    data["totalPoints"] = totalPoints;
    data["members"] = members;
    return data;
  }

  factory Squad.empty(){
    Squad squad = Squad(
      name: "",
      faction: 'Valiants',
      totalPoints: 0,
      members: []
    );
    return squad;
  }
}