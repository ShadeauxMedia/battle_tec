import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'character.g.dart';

@HiveType(typeId: 1)
class Character extends Equatable{
  @HiveField(0)
  String name;
  @HiveField(1)
  String faction;
  @HiveField(2)
  List<String> notes;
  @HiveField(3)
  int points;
  @HiveField(4)
  int energy;
  @HiveField(5)
  int stratgeyRating;
  @HiveField(6)
  int movement;
  @HiveField(7)
  int damage;
  @HiveField(8)
  int rangedAttack;
  @HiveField(9)
  int closeAttack;
  @HiveField(10)
  int shields;


  Character({
    @required this.name, 
    @required this.faction, 
    @required this.notes, 
    @required this.points,
    @required this.energy, 
    @required this.stratgeyRating,
    @required this.movement,
    @required this.damage,
    @required this.rangedAttack, 
    @required this.closeAttack,
    @required this.shields
  });

  @override
  List<Object> get props => [name];


  factory Character.empty(){
    Character character = Character(
      name: "",
      faction: "",
      energy: 0,
      notes: [],
      points:0,
      damage: 0,
      stratgeyRating: 0,
      movement: 0,
      rangedAttack: 0,
      closeAttack: 0,
      shields: 0
    );
    return character;
  }
  factory Character.fromJSON(Map<String, dynamic> json){
    var notes = json['notes'] as List;

    Character character = Character(
      name: json['name'],
      faction: json['faction'],
      energy: json['energy'],
      notes: notes.cast<String>(),
      points: json['points'],
      damage: json['D'],
      stratgeyRating: json['SR'],
      movement: json['M'],
      rangedAttack: json['RA'],
      closeAttack: json['CA'],
      shields: json['SH']
    );
    return character;
  }

  Map<String, dynamic> toJSON(){
    Map<String, dynamic> data = {};
    data['name'] = this.name;
      data['faction'] = this.faction;
      data['energy'] = this.energy;
      data['notes'] = this.notes;
      data['points'] = this.points;
      data['D'] = this.damage;
      data['SR'] = this.stratgeyRating;
      data['M'] = this.movement;
      data['RA'] = this.rangedAttack;
      data['CA'] = this.closeAttack;
      data['SH'] = this.shields;
    return data;
  }

}
