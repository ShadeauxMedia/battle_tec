// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'squad.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SquadAdapter extends TypeAdapter<Squad> {
  @override
  final typeId = 2;

  @override
  Squad read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Squad(
      name: fields[0] as String,
      faction: fields[1] as String,
      totalPoints: fields[2] as int,
      members: (fields[3] as List)?.cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, Squad obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.faction)
      ..writeByte(2)
      ..write(obj.totalPoints)
      ..writeByte(3)
      ..write(obj.members);
  }
}
