import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:hive/hive.dart';

part 'ability.g.dart';

@HiveType(typeId: 0)
class Ability extends Equatable{

  @HiveField(0)
  String name;
  @HiveField(1)
  String desciption;
  @HiveField(2)
  bool isSuper;
  @HiveField(3)
  int cost;

  Ability({this.name, this.desciption, this.isSuper, this.cost});

  @override
  // TODO: implement props
  List<Object> get props => [name];

  factory Ability.empty(){
    return Ability(cost: 0, isSuper: false, name: "", desciption: "");
  }

  factory Ability.fromJSON(Map<String, dynamic> json){
    return Ability(
      name: json['name'],
      desciption: json['description'],
      cost: json['cost'],
      isSuper: json['super']
    );
  }

  Map<String, dynamic> toJson(){
    Map<String, dynamic> data = {};
    data['name'] = this.name;
    data['description'] = this.desciption;
    data['cost'] = this.cost;
    data['super'] = this.isSuper;
    return data;
  }
}