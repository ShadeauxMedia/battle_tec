import 'package:flutter/material.dart';

const DATABASE_PATH = "";

const String ABILITIES_BOX_NAME = "Abliities";
const String FACTIONS_BOX_NAME = "Factions";
const String CHARACTERS_BOX_NAME = "Characters";
const String SQUADS_BOX_NAME = "Squads";

const String ABILITY_FIRST_USE_KEY =
    "com.shadeauxmedia.battle_tec.ability.first_use";
const String FACTION_FIRST_USE_KEY =
    "com.shadeauxmedia.battle_tec.faction.first_use";
const String CHARACTER_FIRST_USE_KEY =
    "com.shadeauxmedia.battle_tec.character.first_use";
const String SQUAD_FIRST_USE_KEY =
    "com.shadeauxmedia.battle_tec.squad.first_use";

const BGD = BoxDecoration(
  image: DecorationImage(
    image: AssetImage("assets/backgrounds/blueDigital.jpg"),
    fit: BoxFit.cover,
  ),
);

const TITLE_FONT_STYLE = TextStyle(
  fontFamily: 'Vector Sigma',
  fontSize: 25,
);
const MEUN_FONT_STYLE = TextStyle(
  fontFamily: 'Vector Sigma',
  fontSize: 18,
);

const FONT_LINK_STYLE =
    TextStyle(color: Colors.red, fontWeight: FontWeight.bold);

void showAlertDialog(
    {BuildContext context, String title, String message, Function okPressed}) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          FlatButton(
            child: Text("Ok"),
            onPressed: () {
              okPressed();
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
