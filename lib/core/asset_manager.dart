import 'package:flutter/services.dart' show rootBundle;

abstract class AssetManager {

  Future<String> loadAsset(String fileName) async {
    return await rootBundle.loadString(fileName);
  }
}