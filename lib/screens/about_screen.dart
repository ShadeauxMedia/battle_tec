import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constiants.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  String appName;
  String packageName;
  String version;
  String buildNumber;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadProperties();
  }

  Future<void> loadProperties() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      appName = packageInfo.appName;
      version = packageInfo.version;
      buildNumber = packageInfo.buildNumber;
      packageName = packageInfo.packageName;
    });
  }

  void openLink(link) async {
    if (await canLaunch(link.url)) {
      await launch(link.url);
    } else {
      throw 'Could not launch $link';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About", style: TITLE_FONT_STYLE,),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BGD,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              "Bot War: Battle Tec",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                fontFamily: 'Vector Sigma'
              ),
            ),
            Text(
              "Version: $version Build: $buildNumber",
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 80),
            Linkify(
              onOpen: openLink,
              text:
                  "Bot War\nCopyright: 2020 Traders Galaxy \nhttps://tradersgalaxy.com.au \nUsed with permission",
              linkStyle: FONT_LINK_STYLE,
              // style: TextStyle(
              //   backgroundColor: Color.fromRGBO(255, 255, 255, 0.3),
              // ),
            ),
            SizedBox(height: 10),
            Linkify(
              onOpen: openLink,
              text:
                  "Battle Tec \nCopyright 2020 Shadeaux Media\nhttp://www.shadeauxmedia.com",
              linkStyle: FONT_LINK_STYLE,
            ),
            //SizedBox(height: 50),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage("assets/images/Robot_05.png"),
                    width: 200,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
