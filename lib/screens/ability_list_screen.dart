import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/ability.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:battle_tec/screens/ability_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';

class AbilityListScreen extends StatelessWidget {
  const AbilityListScreen({Key key}) : super(key: key);

  void _showEditSheet({BuildContext context, bool isNew = true}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => SingleChildScrollView(
        child: Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: AbilityEditScreen(
            isNew: isNew,
          ), //*********** */
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AbilityProvider>(
      builder: (BuildContext context, AbilityProvider value, Widget child) {
        if (value == null) return Container();
        return Scaffold(
          appBar: AppBar(
            title: Text("Abilities", style: TITLE_FONT_STYLE,),
          ),
          body: Container(
            decoration: BGD,
            child: ListView.builder(
              itemCount: value.count,
              itemBuilder: (context, index) {
                Ability ability = value.abilities[index];
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  child: Card(
                    color: Color.fromRGBO(0, 0, 0, 0.3),
                    child: ListTile(
                      onTap: (){
                        value.setSelected(ability.name.hashCode);
                        _showEditSheet(context: context, isNew: false);
                      },
                      title: Text(
                        "${ability.name}",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight:
                                ability.isSuper ? FontWeight.bold : null),
                      ),
                      subtitle: Text(
                          "\n${ability.desciption} \n\n energy cost: ${ability.cost}"),
                    ),
                  ),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        value.deleteAbility(ability.name.hashCode);
                      },
                    ),
                  ],
                );
              },
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Image(
              image: AssetImage('assets/icons/plus_t.png'),
              width: 90,
            ),
            onPressed: () {
              _showEditSheet(context: context);
              //CharacterEditScreen(),
            },
          ),
        );
      },
    );
  }
}
