import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/squad.dart';
import 'package:battle_tec/screens/squad_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:battle_tec/providers/squad_provider.dart';
import '../constiants.dart';

class SquadListScreen extends StatelessWidget {
  const SquadListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SquadProvider>(
      builder: (BuildContext context, SquadProvider provider, Widget child) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Squads", 
              style: TextStyle(
                fontFamily: 'Vector Sigma',
                fontSize: 30,
              ),
            ),
          ),
          body: Container(
            decoration: BGD,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      Squad squad = provider.squads[index];
                      return Card(
                        child:
                            Column(mainAxisSize: MainAxisSize.min, children: <
                                Widget>[
                          ListTile(
                            leading: Image(
                              image: AssetImage(
                                  'assets/factions/${squad.faction}.png'),
                              width: 50,
                            ),
                            title: Text(
                              squad.name,
                              style: TextStyle(
                                fontSize: 24,
                              ),
                            ),
                            trailing: Text("${squad.totalPoints}"),
                          ),
                          ButtonBar(
                            children: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  provider.setSelected(squad.name.hashCode);
                                  Navigator.pushNamed(context, "battle");
                                },
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          'assets/icons/sword_t.png'),
                                      width: 30,
                                    ),
                                    Text("Battle"),
                                  ],
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  provider.setSelected(squad.name.hashCode);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SquadEditScreen(
                                              squad: provider.getSelected())));
                                },
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image:
                                          AssetImage('assets/icons/fix_t.png'),
                                      width: 30,
                                    ),
                                    Text("Edit"),
                                  ],
                                ),
                              ),
                              FlatButton(
                                onPressed: () {
                                  showAlertDialog(
                                      context: context,
                                      title: "Delete Squad",
                                      message:
                                          "Are you sure you want to delete ${squad.name}?",
                                      okPressed: () {
                                        provider.deleteSquad(squad);
                                      });
                                  //Navigator.pushNamed(context, "battle");
                                },
                                child: Row(
                                  children: <Widget>[
                                    Image(
                                      image:
                                          AssetImage('assets/icons/exit_t.png'),
                                      width: 30,
                                    ),
                                    Text("Delete"),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ]),
                      );
                    },
                    itemCount: provider.squads.length,
                  ),
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, "squad_edit");
            },
            child: Image(
              image: AssetImage('assets/icons/plus_t.png'),
              width: 90,
            ),
          ),
          drawer: Drawer(
            child: ListView(
              children: <Widget>[
                DrawerHeader(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/backgrounds/blueDigital.jpg"),
                        fit: BoxFit.cover,
                      ),
                      color: Colors.red,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/Robot_05_sm.png'),
                          width: 110,
                        ),
                        Text(
                          "Bot War: Battle Tec",
                          style: TextStyle(
                            fontSize: 20,
                            fontFamily: "Vector Sigma"
                          ),
                        ),
                        // Text(
                        //   "Battle Tec",
                        //   style: TextStyle(
                        //     fontSize: 16,
                        //   ),
                        // ),
                      ],
                    )),
                ListTile(
                  title: Text(
                    "Squads", 
                    style: MEUN_FONT_STYLE,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, "squads");
                  },
                ),
                ListTile(
                  title: Text("Characters", 
                    style: MEUN_FONT_STYLE,),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, "characters");
                  },
                ),
                ListTile(
                  title: Text("Abilities", 
                    style: MEUN_FONT_STYLE,),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, "abilities");
                  },
                ),
                ListTile(
                  title: Text("About", 
                    style: MEUN_FONT_STYLE,),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, "about");
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
