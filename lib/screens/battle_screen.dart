import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/character.dart';
import 'package:battle_tec/models/squad.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:battle_tec/providers/squad_provider.dart';
import 'package:battle_tec/widgets/character_ability_widget.dart';
import 'package:battle_tec/widgets/character_display_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BattleCharacter {
  final Character character;
  int health;

  bool get isDead {
    return health <= 0;
  }

  BattleCharacter({this.character, this.health});
}

class BattleScreen extends StatefulWidget {
  BattleScreen({Key key}) : super(key: key);

  @override
  _BattleScreenState createState() => _BattleScreenState();
}

class _BattleScreenState extends State<BattleScreen> {
  int totalEnergy = 0;
  Squad squad;
  List<BattleCharacter> team = [];

  @override
  void initState() {
    super.initState();
  }

  void loadCharacters() {
    List<Character> characters = Provider.of<CharacterProvider>(context)
        .filterCharactersByNames(squad.members);
    characters.forEach((character) {
      BattleCharacter bc =
          BattleCharacter(character: character, health: character.damage);
      totalEnergy += character.energy;
      team.add(bc);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (team.length == 0) {
      squad = Provider.of<SquadProvider>(context).getSelected();
      loadCharacters();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Battle",
          style: TITLE_FONT_STYLE,
        ),
      ),
      body: Container(
        decoration: BGD,
        child: Column(
          children: <Widget>[
            Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(
                      squad.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 32,
                      ),
                    ),
                    subtitle: Row(
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 100,
                        ),
                        Image(
                          image: AssetImage('assets/icons/gem_t.png'),
                          width: 30,
                        ),
                        Text(
                          "${totalEnergy}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    color: team[index].isDead ? Colors.red : null,
                    child: Column(mainAxisSize: MainAxisSize.min, children: <
                        Widget>[
                      ListTile(
                        // leading: Image(
                        //   image: AssetImage(
                        //       'assets/factions/${provider.squads[index].faction}.png'),
                        //   width: 50,
                        // ),
                        title: Text(
                          team[index].character.name,
                          style: TextStyle(
                            decoration: team[index].isDead
                                ? TextDecoration.lineThrough
                                : null,
                            fontSize: 24,
                          ),
                        ),
                        subtitle: Row(
                          children: <Widget>[
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 10),
                            Image(
                              image: AssetImage('assets/icons/curs_01_t.png'),
                              width: 30,
                            ),
                            Text(
                              "${team[index].character.movement}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 10),
                            Image(
                              image: AssetImage('assets/icons/target_t.png'),
                              width: 30,
                            ),
                            Text(
                              "${team[index].character.rangedAttack}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 10),
                            Image(
                              image: AssetImage('assets/icons/war_t.png'),
                              width: 30,
                            ),
                            Text(
                              "${team[index].character.closeAttack}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width / 10),
                            Image(
                              image: AssetImage('assets/icons/armor_t.png'),
                              width: 30,
                            ),
                            Text(
                              "${team[index].character.shields}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        // trailing:
                        //     Text("${team[index].character.stratgeyRating}"),
                        onTap: () {
                          showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            builder: (context) => SingleChildScrollView(
                              child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom),
                                  child: Container(
                                      child: CharacterAbilityWidget(
                                    character: team[index].character,
                                    showAsAlert: true,
                                  ))),
                            ),
                          );
                        },
                      ),
                      ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            onPressed: !team[index].isDead
                                ? () {
                                    setState(() {
                                      team[index].health--;
                                    });
                                    if (team[index].isDead) {
                                      totalEnergy -=
                                          team[index].character.energy;
                                    }
                                  }
                                : null,
                            child: Row(
                              children: <Widget>[
                                Image(
                                  image: AssetImage('assets/icons/left_t.png'),
                                  width: 50,
                                ),
                                //Text("up"),
                              ],
                            ),
                          ),
                          Text(
                            "${team[index].health}",
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          FlatButton(
                            onPressed: () {
                              if (team[index].isDead)
                                totalEnergy += team[index].character.energy;
                              setState(() {
                                team[index].health++;
                              });
                            },
                            child: Row(
                              children: <Widget>[
                                Image(
                                  image: AssetImage('assets/icons/right_t.png'),
                                  width: 50,
                                ),
                                //Text("down"),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ]),
                  );
                },
                itemCount: team.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
