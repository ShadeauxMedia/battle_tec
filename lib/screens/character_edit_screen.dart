import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/ability.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:flutter/material.dart';
import 'package:battle_tec/providers/faction_provider.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:battle_tec/models/character.dart';
import 'package:provider/provider.dart';

class CharacterEditScreen extends StatefulWidget {
  Character character;
  CharacterEditScreen({Key key, this.character}) : super(key: key);

  @override
  _CharacterEditScreenState createState() => _CharacterEditScreenState();
}

class _CharacterEditScreenState extends State<CharacterEditScreen> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController pointsController = TextEditingController();
  final TextEditingController energyController = TextEditingController();
  final TextEditingController srController = TextEditingController();
  final TextEditingController shController = TextEditingController();
  final TextEditingController raController = TextEditingController();
  final TextEditingController caController = TextEditingController();
  final TextEditingController dController = TextEditingController();
  final TextEditingController movementController = TextEditingController();

  String faction = "Valiants";
  Character character;
  @override
  void initState() {
    super.initState();
    loadCharacter();
  }

  void loadCharacter() {
    character = widget.character;
    if (character == null) {
      character = Character.empty();
      faction = "Valiants";
    } else {
      faction = character.faction;
    }
    nameController.text = character.name;
    pointsController.text = "${character.points}";
    energyController.text = "${character.energy}";
    srController.text = "${character.stratgeyRating}";
    shController.text = "${character.stratgeyRating}";
    raController.text = "${character.rangedAttack}";
    caController.text = "${character.closeAttack}";
    dController.text = "${character.damage}";
    movementController.text = "${character.movement}";
  }

  void saveCharacter() {
    character.name = nameController.text;
    character.faction = faction;
    character.points = int.parse(pointsController.text);
    character.energy = int.parse(energyController.text);
    character.stratgeyRating = int.parse(srController.text);
    character.shields = int.parse(shController.text);
    character.rangedAttack = int.parse(raController.text);
    character.closeAttack = int.parse(caController.text);
    character.damage = int.parse(dController.text);
    character.movement = int.parse(movementController.text);
  }

  final List<Tab> tabs = [
    Tab(text: 'Stats'),
    Tab(text: 'Abilities'),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Character Edit", style: TITLE_FONT_STYLE,),
          bottom: TabBar(tabs: tabs),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                saveCharacter();
                Provider.of<CharacterProvider>(context, listen: false)
                    .addCharacter(character);
                Navigator.pop(context);
              },
              child: Icon(
                Icons.save,
                color: Colors.white,
              ),
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: TabBarView(
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    TextField(
                      controller: nameController,
                      decoration: InputDecoration(labelText: "Name"),
                    ),
                    Row(
                      children: <Widget>[
                        Text("Faction:"),
                        DropdownButton<String>(
                          value: faction,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          underline: Container(
                            height: 2,
                            color: Colors.redAccent,
                          ),
                          onChanged: (String newValue) {
                            setState(() {
                              faction = newValue;
                            });
                          },
                          items: Provider.of<FactionProvider>(context)
                              .factions
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/money_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: pointsController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Points:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/gem_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: energyController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Entergy:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/rank_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: srController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Stratgey Rating:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/curs_01_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: movementController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Movement:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/armor_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: shController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Shields:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/target_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: raController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Ranged Attack:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/war_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: caController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Close Attack:",
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        // Points
                        SizedBox(
                          width: 10,
                        ),
                        Image(
                          image: AssetImage('assets/icons/heart_t.png'),
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextField(
                            controller: dController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: "Health:",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("Ability:"),
                      DropdownButton<String>(
                        //value: faction,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        underline: Container(
                          height: 2,
                          color: Colors.redAccent,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            character.notes.add(newValue);
                          });
                        },
                        items: Provider.of<AbilityProvider>(context)
                            .abilityList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: character.notes.length,
                      itemBuilder: (context, index) {
                        Ability ability = Provider.of<AbilityProvider>(context)
                            .getAbilityByName(character.notes[index]);
                        if (ability == null) {
                          ability = Ability.empty();
                        }
                        return ListTile(
                          title: Text(
                            "${ability.name}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: ability.isSuper ? 24 : 18,
                                fontWeight:
                                    ability.isSuper ? FontWeight.bold : null),
                          ),
                          trailing: FlatButton(
                            child: Icon(Icons.close),
                            onPressed: () {
                              setState(() {
                                character.notes.removeAt(index);
                              });
                            },
                          ),
                          onTap: () {
                            SnackBar snackbar = SnackBar(
                              content: Text(ability.desciption),
                            );
                            Scaffold.of(context).showSnackBar(snackbar);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
