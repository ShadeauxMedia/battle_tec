import 'package:battle_tec/widgets/splash.dart';
import 'package:flutter/material.dart';
import 'squad_list_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 10,
      navigateDesitnation: SquadListScreen(),
      backgroundImage: AssetImage("assets/images/splash.png"),
    );
  }
}
