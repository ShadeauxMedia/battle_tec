import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:battle_tec/screens/character_screen.dart';
import 'character_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/character.dart';
import '../constiants.dart';

class CharacterListScreen extends StatelessWidget {
  CharacterListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CharacterProvider>(
      builder:
          (BuildContext context, CharacterProvider provider, Widget child) {    
        return Scaffold(
          appBar: AppBar(
            title: Text("Characters", style: TITLE_FONT_STYLE,),
          ),
          body: Container(
            decoration: BGD,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                      itemCount: provider.characters.length,
                      itemBuilder: (context, index) {
                        Character character = provider.characters[index];
                        return Card(
                          child: ListTile(
                            title: Text("${character.name}"),
                            subtitle: Text("${character.faction}"),
                            onLongPress: () {
                              showAlertDialog(
                                context: context,
                                title: "Delete Character",
                                message: "Are you sure you want to delete ${character.name}",
                                okPressed: (){
                                  provider.deleteCharacter(character);
                                }
                              );
                            },
                            onTap: (){
                              provider.setSelected(character.name.hashCode);
                              Navigator.push(context, 
                              MaterialPageRoute(
                                builder: (context) => CharacterScreen()),
                              );
                            },
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
        child: Image(
            image: AssetImage('assets/icons/plus_t.png'),
            width: 90,
          ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (contex) => CharacterEditScreen()));
          //CharacterEditScreen(),
        },
      ),
        );
      },
    );
  }
}
