import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:battle_tec/widgets/character_display_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/character.dart';
import '../models/ability.dart';
import 'character_edit_screen.dart';

class CharacterScreen extends StatelessWidget {
  
  //final Character character; 
  CharacterScreen({Key key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    Character character = Provider.of<CharacterProvider>(context).getSelected();
    return Scaffold(
      appBar: AppBar(
        title: Text("Character Data", style: TITLE_FONT_STYLE,),
      ),
      body: Container(
        decoration: BGD,
        child: CharacterDisplayWidget(character: character),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          // showModalBottomSheet(
          //   context: context,
          //   isScrollControlled: true,
          //   builder: (context) => SingleChildScrollView(
          //     child: Container(
          //       padding: EdgeInsets.only(
          //           bottom: MediaQuery.of(context).viewInsets.bottom),
          //       child: CharacterEditScreen(character: character,),
          //     ),
          //   ),
          // );
          Navigator.push(context, MaterialPageRoute(builder: (contex) => CharacterEditScreen(character: character,)));
        },
        child: Image(
              image: AssetImage('assets/icons/pencil_t.png'),
              width: 90,
            ),
      ),
    );
  }
}


