import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/character.dart';
import 'package:battle_tec/models/squad.dart';
import 'package:battle_tec/providers/character_provider.dart';
import 'package:battle_tec/providers/faction_provider.dart';
import 'package:battle_tec/providers/squad_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SquadEditScreen extends StatefulWidget {
  
  Squad squad;
  
  SquadEditScreen({Key key, this.squad}) : super(key: key);
  
  @override
  _SquadEditScreenState createState() => _SquadEditScreenState();
}

class _SquadEditScreenState extends State<SquadEditScreen> {

  @override
  void initState() { 
    super.initState();
    //loadSquad();
  }

  TextEditingController nameController = TextEditingController();
  String faction = "Valiants";
  int totalCost = 0;
  List<Character> members = [];
  bool firstLoad = true;

  void loadSquad(){
    if(widget.squad != null){
      nameController.text = widget.squad.name;
      faction = widget.squad.faction;
      totalCost = widget.squad.totalPoints;
      members.addAll(Provider.of<CharacterProvider>(context).filterCharactersByNames(widget.squad.members));
    }
  }

  void saveSquad(){
    Squad squad = widget.squad;
    if(squad == null)
      squad = Squad.empty();
    squad.name = nameController.text;
    squad.faction = faction;
    squad.totalPoints = totalCost;
    squad.members.clear();
    members.forEach((character) => squad.members.add(character.name) );
    Provider.of<SquadProvider>(context, listen: false).addSquad(squad);
  }

  @override
  Widget build(BuildContext context) {
    
    
      if(members.length == 0)
        loadSquad();
    

    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Squad", style: TITLE_FONT_STYLE,),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                saveSquad();
                Navigator.pop(context);
              },
              child: Icon(
                Icons.save,
                color: Colors.white,
              ),
            ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: <Widget>[
            TextField(
              controller: nameController,
              decoration: InputDecoration(labelText: "Name"),
            ),
            Row(
              children: <Widget>[
                Text("Faction:"),
                SizedBox(width: 10),
                DropdownButton<String>(
                  value: faction,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  underline: Container(
                    height: 2,
                    color: Colors.redAccent,
                  ),
                  onChanged: (String newValue) {
                    setState(() {
                      faction = newValue;
                    });
                  },
                  items: Provider.of<FactionProvider>(context)
                      .factions
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ],
            ),
             Row(
              children: <Widget>[
                Text("Total Cost:"),
                SizedBox(width: 10),
                Text("$totalCost"),
              ],
            ),
            Row(
                    children: <Widget>[
                      Text("Members:"),
                      DropdownButton<Character>(
                        //value: faction,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        underline: Container(
                          height: 2,
                          color: Colors.redAccent,
                        ),
                        onChanged: (Character newValue) {
                          setState(() {
                            totalCost += newValue.points;
                            members.add(newValue);
                          });
                        },
                        items: Provider.of<CharacterProvider>(context)
                            .characters
                            .map<DropdownMenuItem<Character>>((Character value) {
                          return DropdownMenuItem<Character>(
                            value: value,
                            child: Text(value.name),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: members.length,
                      itemBuilder: (context, index) {
                        Character character = members[index];
                        return ListTile(
                          title: Text(
                            "${character.name}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                ),
                          ),
                          trailing: FlatButton(
                            child: Icon(Icons.close),
                            onPressed: (){
                              setState(() {
                                totalCost -= character.points;
                                members.removeAt(index);
                              });
                            },
                          ),
                         
                        );
                      },
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
