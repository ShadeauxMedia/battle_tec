import 'package:battle_tec/constiants.dart';
import 'package:battle_tec/models/ability.dart';
import 'package:battle_tec/providers/ability_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AbilityEditScreen extends StatefulWidget {
  final bool isNew;
  const AbilityEditScreen({Key key, this.isNew}) : super(key: key);

  @override
  _AbilityEditScreenState createState() => _AbilityEditScreenState();
}

class _AbilityEditScreenState extends State<AbilityEditScreen> {

  TextEditingController nameController = TextEditingController();
  TextEditingController energyController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  bool isSuper = false;
  Ability ability;


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loadAbility();
  }

  void _loadAbility(){
    if(widget.isNew)
      ability = Ability.empty();
    else
      ability = Provider.of<AbilityProvider>(context, listen: false).getSelected();
    
    setState(() {
      nameController.text = ability.name;
      descriptionController.text = ability.desciption;
      isSuper = ability.isSuper;
      energyController.text = "${ability.cost}";
    });
  }

  void _saveAbility(){
    ability.name = nameController.text;
    ability.desciption = descriptionController.text;
    ability.cost = int.parse(energyController.text);
    ability.isSuper = isSuper;
    Provider.of<AbilityProvider>(context, listen: false).saveAbility(ability);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Container(
      //padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text("Edit Ability", style: TITLE_FONT_STYLE,),
            automaticallyImplyLeading: false,
            actions: <Widget>[
            FlatButton(
              onPressed: () {
               _saveAbility();
                Navigator.pop(context);
              },
              child: Icon(
                Icons.save,
                color: Colors.white,
              ),
            ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: "Name",
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text("Super Ability"),
                    Checkbox(
                      activeColor: Colors.red,
                      value: isSuper,
                      onChanged: (newValue) {
                        setState(() {
                          isSuper = newValue;
                        });
                      },
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/icons/gem_t.png'),
                      width: 30,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Flexible(
                      flex: 1,
                      child: TextField(
                        controller: energyController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: "Entergy cost:",
                        ),
                      ),
                    ),
                  ],
                ),
                TextField(
                  controller: descriptionController,
                  minLines: 5,
                  maxLines: 10,
                  expands: false,
                  decoration: InputDecoration(
                    labelText: "Description",
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
