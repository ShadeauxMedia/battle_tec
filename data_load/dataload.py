from firebase import firebase
import json
import sys

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("file", help="json file to parse")
parser.add_argument("-c", "--collection", action='store', default='characters', help="set collection to write data to")
args = parser.parse_args()

file = args.file
table_name = "/{}".format(args.collection)
# https://console.firebase.google.com/project/botwarbattletrac/database/firestore/data/
firebase = firebase.FirebaseApplication('https://botwarbattletrac.firebaseio.com')
print("uploading into table: {}".format(table_name))
def process_char(character):
    id = firebase.post(table_name, character)
    if 'name' in character:
        print("\t{0} - {1}".format(id, character['name']))
    else: 
        print("\t{0} - {1}".format(id, character))

with open(file) as json_file:
    data = json.load(json_file)
    [process_char(character) for character in data]

